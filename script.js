const themeMap = {
    dark: "light",
    light: "solar",
    solar: "dark"
};

const themeToImage = {
    dark: "avatar-dark.png",
    light: "avatar-light.png",
    solar: "avatar-solar.png"
};

const script = localStorage.getItem('script') ?? defaultTheme();
const bodyClass = document.body.classList;
bodyClass.add(script);
setProfilePicture(script);

function defaultTheme() {
    const theme = Object.keys(themeMap)[0];
    localStorage.setItem('script', theme);
    return theme;
}

function toggleTheme() {
    const current = localStorage.getItem('script');
    const next = themeMap[current];

    bodyClass.replace(current, next);
    localStorage.setItem('script', next);

    setProfilePicture(next);
}

function setProfilePicture(theme) {
    let pfps = document.getElementsByClassName("profile-picture");
    for (let pfp of pfps) {
        pfp.src = `../assets/${themeToImage[theme]}`;
    }
}

/* calculate age from birthday */
function calculateAge() {
    const birthDate = new Date(2001, 7, 12);
    const today = new Date();

    let difference = today - birthDate.getTime();

    let age = Math.abs(new Date(difference).getUTCFullYear() - 1970);

    for (let tag of document.getElementsByClassName("age")) {
        tag.innerHTML = `${age}`;
    }
}

calculateAge();